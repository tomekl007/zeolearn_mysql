use test;

# group by points per lastname
SELECT 
	lastname
FROM
    players_with_points
GROUP BY lastname;

# THIS WILL FAIL - you cannot select column that was not used for group by
SELECT 
	lastname,
	id
FROM
    players_with_points
GROUP BY lastname;

# group by points per lastname and count
SELECT 
	lastname,
	COUNT(lastname)
FROM
    players_with_points
GROUP BY lastname;

# group by and get stats about the field
SELECT 
	lastname,
	AVG(points),
	MIN(points),
	MAX(points)
FROM
    players_with_points
GROUP BY lastname;

# group by and get stats about the city field
SELECT 
	city,
	AVG(points),
	MIN(points),
	MAX(points)
FROM
    players_with_points
GROUP BY city;