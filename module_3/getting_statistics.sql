use test;

CREATE TABLE players_with_points(
   id INT AUTO_INCREMENT PRIMARY KEY,
   lastname VARCHAR(40),
   points INT,
   city VARCHAR(255)
);

# insert data
insert into players_with_points values(1, "Mike", 100, "Colorado");
insert into players_with_points values(2, "Mike", 10, "Paris");
insert into players_with_points values(3, "Chris", 32, "Paris");
insert into players_with_points values(4, "Alex", 22, null);
insert into players_with_points values(5, "John", 99, null);

# calculate average number of points
SELECT 
    AVG(points) points_average
FROM 
    players_with_points;

# sum all values
SELECT 
    SUM(points) sum_of_all_points
FROM 
    players_with_points;    

# MAX 
SELECT 
    MAX(points) max_value
FROM 
    players_with_points;        

# MIN
SELECT 
    MIN(points) min_value
FROM 
    players_with_points;            