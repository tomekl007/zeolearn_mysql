use test;

# use having as the "where" for groups for one column
SELECT city FROM 
	players_with_points 
GROUP BY city HAVING LENGTH(city) >= 5;

# use having as the "where" for two groups
SELECT city, points FROM 
	players_with_points 
GROUP BY city, points HAVING points > 30;

# having on aggregate funtion
SELECT city, AVG(points) as points_avg FROM 
	players_with_points 
GROUP BY city, points HAVING points_avg > 30;