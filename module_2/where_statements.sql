USE test;

# simple where
SELECT 
    lastname, 
    city
FROM
    players
WHERE
    city = 'Paris';

# where with AND
SELECT 
    lastname, 
    city
FROM
    players
WHERE
    city = 'Paris' AND 
    lastname = 'Chris';    

# where with OR
SELECT 
    lastname, 
    city
FROM
    players
WHERE
    city = 'Paris' OR
    lastname = 'Mike';    

# where with LIKE (city with r)
SELECT 
    lastname, 
    city
FROM
    players
WHERE
    city LIKE '%r%';       

# where not null
SELECT 
    lastname, 
    city
FROM
    players
WHERE
    city IS NULL;
