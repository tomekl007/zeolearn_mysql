use test;

# default for orderby is ascending 
SELECT
    *
FROM
    players_with_points
ORDER BY
    points;

SELECT
    *
FROM
    players_with_points
ORDER BY
    points ASC;    

# DESC
SELECT
    *
FROM
    players_with_points
ORDER BY
    points DESC;    

# multiple columns    
SELECT
    *
FROM
    players_with_points
ORDER BY
    lastname ASC,
    points DESC;  

# order by result of funciton
SELECT 
    lastname, 
    points, 
    LENGTH(lastname)
FROM
    players_with_points
ORDER BY 
   LENGTH(lastname) DESC;

# order by provided list
SELECT 
    *
FROM
    players_with_points
ORDER BY 
    FIELD(city,
        'Paris',
        'Colorado') ASC;
