use test;

# simple count
SELECT 
	COUNT(*)
FROM 
	players;	

# count with WHERE	
SELECT 
	COUNT(*)
FROM 
	players
WHERE 
	city = 'Paris';		

# count with DISTINCT
SELECT 
	COUNT(DISTINCT(lastname))
FROM 
	players;

# count with function
SELECT 
    COUNT(IF(lastname = 'Mike', 1, NULL)) 'number of Mike',
    COUNT(IF(city = 'Paris', 1, NULL)) 'number of Paris'
FROM
    players;	


# limit number of results
SELECT 
    *
FROM
    players
LIMIT 2;    

# limit with offset for pagination (page with 2 values)
# first page
SELECT 
    *
FROM
    players
LIMIT 0, 2;

# second page
SELECT 
    *
FROM
    players
LIMIT 2, 2;

# third page ....

SELECT 
    *
FROM
    players
LIMIT 4, 2;