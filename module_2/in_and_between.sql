use test;


CREATE TABLE players_with_points(
   id INT AUTO_INCREMENT PRIMARY KEY,
   lastname VARCHAR(40),
   points INT,
   city VARCHAR(255)
);

# insert data
insert into players_with_points values(1, "Mike", 100, "Colorado");
insert into players_with_points values(2, "Mike", 10, "Paris");
insert into players_with_points values(3, "Chris", 32, "Paris");
insert into players_with_points values(4, "Alex", 22, null);
insert into players_with_points values(5, "John", 99, null);


# select with IN
SELECT 
	*
FROM
    players_with_points
WHERE
    points IN (10, 22, 30);

# select with BETWEEN
SELECT 
	*
FROM
    players_with_points
WHERE
    points BETWEEN 10 AND 30;

# select all that are not Mike
SELECT 
    *
FROM
    players_with_points
WHERE
    lastname <> 'Mike';

# select all with points greater than or equal
SELECT 
	*
FROM
    players_with_points
WHERE
    points >= 50;
