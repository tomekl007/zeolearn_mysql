USE test;

CREATE TABLE players(
   id INT AUTO_INCREMENT PRIMARY KEY,
   lastname VARCHAR(40),
   city VARCHAR(255)
);

# insert data
insert into players values(1, "Mike", "Colorado");
insert into players values(2, "Mike", "Paris");
insert into players values(3, "Chris", "Paris");
insert into players values(4, "Alex", null);
insert into players values(5, "John", null);

# select lastname 
SELECT lastname FROM players;

# distinct one column
SELECT DISTINCT lastname FROM players;

# note that null will be once 
SELECT DISTINCT city FROM players;
  
# distinct on two columns  
SELECT DISTINCT city, lastname FROM players;
