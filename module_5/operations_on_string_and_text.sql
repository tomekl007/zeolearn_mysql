# some string functions (CONCAT, CONCAT_WS, INSERT, UPPER)
use test;

CREATE TABLE IF NOT EXISTS texts (
  id INT NOT NULL PRIMARY KEY,
  name VARCHAR(50),
  surname VARCHAR(50)
);

INSERT INTO texts values(1, 'Mikael', 'Pool');
INSERT INTO texts values(2, 'Alex', 'Tyson');

# concat without separator
select CONCAT(name, surname) from texts;

# concat with separator
select CONCAT_WS(" ", name, surname) from texts;

# ISNERT OTHER string at an index (at position 2, lenght 3)
SELECT INSERT(name, 2, 3, 'XXX') from texts;

#to UPPER case
SELECT UPPER(name), name from texts;


# REGEX - check if the value matches the provided REGEXP
# it matches anything at index 1
SELECT name REGEXP '.*', name from texts; 

# only matches Mikael
SELECT name REGEXP '.*el', name from texts; 