# ABS
use test;

DROP TABLE IF EXISTS numbers;
CREATE TABLE IF NOT EXISTS numbers (
  id INT NOT NULL,
  value INT,
  floatingNumber FLOAT	 
);

INSERT INTO numbers(id,value) values(1, 10);
INSERT INTO numbers(id,value) values(2, 0);
INSERT INTO numbers(id,value) values(3, -30);
INSERT INTO numbers(id,value) values(4, null);

select * from numbers;

select id, abs(value), value from numbers;

# MOD (mod 2 - check if value can be divided by 2 without a reminder)
INSERT INTO numbers(id,value) values(5, 3);
select id, mod(value, 2), value from numbers;

# mod 3
select id, mod(value, 3), value from numbers;

# FLOOR & CEILING
INSERT INTO numbers(id, floatingNumber) values(6, 1.2);
INSERT INTO numbers(id, floatingNumber) values(7, 3.7);

SELECT id, FLOOR(floatingNumber), floatingNumber FROM numbers;
SELECT id, CEILING(floatingNumber), floatingNumber FROM numbers;
