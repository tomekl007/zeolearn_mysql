use test;

# an employee can be a manager or normal employee. 
# For normal employee the reportsTo will point to another employee that is a manager
CREATE TABLE IF NOT EXISTS employees (
  id INT NOT NULL PRIMARY KEY,
  name VARCHAR(50),
  reportsTo INT
);

# insert manager
INSERT INTO employees values(1, 'Mike John', null);

# insert employees
INSERT INTO employees values(2, 'Alex Worker', 1);
INSERT INTO employees values(3, 'John Worker', 1);

# get company structure using self join (INNER JOIN)
SELECT 
    CONCAT(m.name) AS Manager,
    CONCAT(e.name) AS 'Employee'
FROM
    employees e
INNER JOIN employees m ON 
    m.id = e.reportsTo;


# insert manager for Mike John
UPDATE employees SET reportsTo = 4 where id = 1;
INSERT INTO employees values(4, 'CEO', null);    

# see the structure once again:
SELECT 
    CONCAT(m.name) AS Manager,
    CONCAT(e.name) AS 'Employee'
FROM
    employees e
INNER JOIN employees m ON 
    m.id = e.reportsTo;
