# create test db
create database test;
USE test;
# only Primary key table
CREATE TABLE table_name(
    primary_key_column INT PRIMARY KEY
);

# create users table
CREATE TABLE users(
   user_id INT AUTO_INCREMENT PRIMARY KEY,
   username VARCHAR(40),
   password VARCHAR(255),
   email VARCHAR(255)
)

# auto incremented primary key
CREATE TABLE roles(
   role_id INT AUTO_INCREMENT,
   role_name VARCHAR(50),
   PRIMARY KEY(role_id)
);

# create user_roles table that creates N to N relation
# One user can have multiple roles
# one role can belong to multiple users
CREATE TABLE user_roles(
   user_id INT,
   role_id INT,
   PRIMARY KEY(user_id,role_id),
   FOREIGN KEY(user_id) 
       REFERENCES users(user_id),
   FOREIGN KEY(role_id) 
       REFERENCES roles(role_id)
);

# create new index on email field - we will be able to lookup users very fast via email_unique index on email field
# the ordering of it is ascending
ALTER TABLE users ADD UNIQUE INDEX  email_unique (email ASC);

