USE test;

CREATE TABLE t1 (
    id INT AUTO_INCREMENT,
    title VARCHAR(100) NOT NULL,
    PRIMARY KEY(id)
);

insert into t1 values(1, 'batman');

DROP table t1;


# drop table that does not exists cause an error to happen
DROP table t1;

# to not have error use:
DROP TABLE IF EXISTS t1;

# to see warninigs
SHOW WARNINGS;

# add new column to existing table
ALTER TABLE t1 ADD new_column INT;

# drop column
ALTER TABLE t1 DROP COLUMN new_column;

# change type of the column - if there is data there will be error
ALTER TABLE t1 MODIFY title int;

# remove
DELETE FROM t1;

# change again - it will work
ALTER TABLE t1 MODIFY title int;


