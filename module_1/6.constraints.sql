USE test;


# on insert it will check if cost of the record is above or equal to 0, otherwise it will prohibit inserting the record
CREATE TABLE parts (
    id VARCHAR(18) PRIMARY KEY,
    description VARCHAR(40),
    cost DECIMAL(10,2 ) NOT NULL CHECK (cost >= 0)
);

# see the schema of table
	
SHOW CREATE TABLE parts;
# insert record
INSERT INTO parts(id, description,cost) VALUES('111','some product',-1);


# NOT NULL CONSTRAINT
CREATE TABLE t2(
   user_id INT AUTO_INCREMENT PRIMARY KEY,
   username VARCHAR(40) NOT NULL
);

INSERT INTO t2 VALUES(1, 'v1');
INSERT INTO t2 VALUES(2, 'v1');
# validate that not null works
INSERT INTO t2 VALUES(1, null);

#  add unique on username - dupilcate entry already exists - we need to remove before creating this constraint
ALTER TABLE t2 ADD UNIQUE INDEX username_unique (username ASC);
delete from t2 where user_id = 1;

# create properly
ALTER TABLE t2 ADD UNIQUE INDEX username_unique (username ASC);

# cannot insert now
INSERT INTO t2 VALUES(2, 'v1');