# install mysql
brew install mysql

# start mysql server
mysqld --console

# connect to mysql server using mysql client - type exit to close the client
mysql -u root -p   

# show all databases withing mysql instance
SHOW DATABASES;

# use mysql db
use mysql;

# show tables within mysql db
SHOW TABLES;

# select all from db
select * from db;