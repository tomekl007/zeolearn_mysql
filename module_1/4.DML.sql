# DDL logic

# we will be using schema from previous video
# create test db
create database test;
USE test;
# only Primary key table
CREATE TABLE table_name(
    primary_key_column INT PRIMARY KEY
);

# create users table
CREATE TABLE users(
   user_id INT AUTO_INCREMENT PRIMARY KEY,
   username VARCHAR(40),
   password VARCHAR(255),
   email VARCHAR(255)
)

# auto incremented primary key
CREATE TABLE roles(
   role_id INT AUTO_INCREMENT,
   role_name VARCHAR(50),
   PRIMARY KEY(role_id)
);

# create user_roles table that creates N to N relation
# One user can have multiple roles
# one role can belong to multiple users
CREATE TABLE user_roles(
   user_id INT,
   role_id INT,
   PRIMARY KEY(user_id,role_id),
   FOREIGN KEY(user_id) 
       REFERENCES users(user_id),
   FOREIGN KEY(role_id) 
       REFERENCES roles(role_id)
);

# DML logic

# create new index on email field - we will be able to lookup users very fast via email_unique index on email field
# the ordering of it is ascending
ALTER TABLE users ADD UNIQUE INDEX  email_unique (email ASC);


# insert data into table_name
insert into table_name(primary_key_column) VALUES (1);
# try to insert duplicate - should get an error
insert into table_name(primary_key_column) VALUES (1);
select * from table_name;  

# insert into users - when all values are inserted the names of columns could be omited
insert into users values(2, 'a','p', 'e@com');
select * from users;

# PK is auto-incremented so it can be omited
insert into users(username,password,email) values('b','p', 'e2@com');
# note auto-incremented id 
select * from users;  

# insert duplicate email - it cannot be done because we have unique index on email column
insert into users(username,password,email) values('b','p', 'e2@com');

# where will narrow update to only records that have username = a 
update users SET username = 'a_updated' where username ='a';

# WARNING: when running without where, ALL RECORDS ARE UPDATED
update users SET username = 'a_updated2';
select * from users;  

delete from users where username ='a';

# WARNINIG: all records will be deleted
delete from users;

select * from users;  



