# LEFT JOIN
use test;

# create first table with PK
CREATE TABLE IF NOT EXISTS product (
  productCode CHAR(3) PRIMARY KEY,
  description VARCHAR(50)
);


# create purchase that has relation to product using foreign key productCode
CREATE TABLE IF NOT EXISTS purchase (
  productId    	 INT UNSIGNED  NOT NULL AUTO_INCREMENT,
  productCode  	 CHAR(3)       NOT NULL DEFAULT '',
  quantity       INT UNSIGNED  NOT NULL DEFAULT 0,
  PRIMARY KEY  (productID),
  FOREIGN KEY (productCode) REFERENCES product(productCode)
);

# insert some data
insert into product values('API', 'Apple Iphone');
insert into product values('WWO', 'Wide Screen');
insert into product values('SON', 'Sony TV');

# inser data that references it
insert into purchase values(1, 'API', 10);
insert into purchase values(2, 'WWO', 30);
# this one will fail because you cannot insert record for non existing product code
insert into purchase values(3, 'MPO', 22);	

# perform left join (all products - include not matching purchases)
SELECT 
    p1.productCode,
    p1.description,
    p2.quantity
FROM
    product p1
LEFT JOIN purchase p2
    ON p1.productCode = p2.productCode;

# perform righ join (all purchases)
SELECT 
    p1.productCode,
    p1.description,
    p2.quantity
FROM
    product p1
RIGHT JOIN purchase p2
    ON p1.productCode = p2.productCode;

# right join with switched tables - same behavior as left join
SELECT 
    p2.productCode,
    p2.description,
    p1.quantity
FROM
    purchase p1
RIGHT JOIN product p2
    ON p1.productCode = p2.productCode;