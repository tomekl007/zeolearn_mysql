 
# Query that reqires a lot of work from MySql
DELETE FROM products WHERE created < DATE_SUB(NOW(),INTERVAL 6 MONTH);

# you can split it into 
rows_deleted = 0
do {
   rows_deleted = do_query(
      "DELETE FROM products WHERE created < DATE_SUB(NOW(),INTERVAL 6 MONTH)
      LIMIT 10000")
} while rows_deleted > 0