mysqld --console

mysql -u root -p   

# validate that none of the passwords expired
SELECT User,Host,password_last_changed,password_expired FROM mysql.user;

# create db
create database testDB;

# create new dedicated user:
CREATE USER 'demo-user'@'localhost' IDENTIFIED BY 'password';

# revoke some permissions
REVOKE UPDATE ON testDB.* FROM 'demo-user'@'localhost';

# grant all
GRANT ALL ON testDB.* TO 'demo-user'@'localhost';

# show all privilidges:
FLUSH PRIVILEGES;
show grants for 'demo-user'@'localhost';

# Changing the ROOT user
rename user 'root'@'localhost' to 'newAdminUser'@'localhost';

# validate
SELECT User,Host,password_last_changed,password_expired FROM mysql.user;

# flush 
FLUSH PRIVILEGES;

# use new user when logging
mysql -u newAdminUser -p


# enable TLS
grant all privileges on mydb.* to 'user'@'localhost' identified by ‘astrongpassword’ REQUIRE SSL;

flush privileges;
