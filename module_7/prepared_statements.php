# --------- Traditional Escaping --------

# get data submitted by the user
$email_from_post = $_POST['email'];

# escape the email_from_post
# to prevent malicious attacks explained earlier in this video
$sanitizedEmail = $db->real_escape_string($email_from_post);

# Run the query
$query = "SELECT * FROM `user` WHERE `email`='{$sanitizedEmail}'";
$result = $db->query($query);

# Loop over the results
while (($user = $result->fetch_assoc()) != null)
{
    print "User: " . print_r($user, true) . PHP_EOL;
}


# --------- Escaping using Prepared statements --------

# get posted form inputs
$rawEmail = $_POST['email'];

# Prepare the statement - notice the ? and no variables yet.
$select = "SELECT * FROM `user` WHERE `email`= ?";
$stmt = $db->prepare($select);

# Bind the user-provided parameters to the compiled prepared statement
$stmt->bind_param("s", $rawEmail);

# Execute the statement
$stmt->execute();

# Get the result
$result = $stmt->get_result();

# loop over the rows returned in result (I am assuming didn't return FALSE for error)
while (($user = $result->fetch_assoc()) ! == null)
{
    print "User: " . print_r($user, true) . PHP_EOL;
}

$stmt->close();
