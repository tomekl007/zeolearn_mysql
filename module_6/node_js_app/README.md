# node_js_mysql app:

1. start MySql server 
2. Execute database/db.sql script on your MySql instance
3. `npm install`
4. `nodemon src/index.js`
5. `http://localhost:2001/`
6. save record
7. verify that record is in the mysql:
   `select * from nodejs2.customer;`


In case of a problem with error:

"Client does not support authentication protocol requested by server; consider upgrading MySQL client"

execute this in the mysql console:

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password'
