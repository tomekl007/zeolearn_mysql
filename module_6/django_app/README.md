Django-MySQL-Example
=========

A simple example, based on Python3 and MySQL, upload and show multimedia, like musics, pictures or videos.

Getting started
----------------
1.Create install python, pip, virtual env and install django:
```
    brew install python
    brew install pyenv-virtualenv
    # or
    sudo pip install virtualenv
    pyenv virtualenv django     
    pyenv activate django
    pip install django
    pip install pymysql
    pip install Pillow
```

2.execute sql on the mysql database using client
```
    CREATE DATABASE socialmedia;
    CREATE TABLE socialmedia.myweb_img ( id INT AUTO_INCREMENT PRIMARY KEY, title VARCHAR(128), img BLOB NOT NULL);
```


3.Start Django Server:
```python
python manage.py runserver
```
4.View the webside and upload a file:

```
http://127.0.0.1:8000/upload
```

verify that image was uploaded:
`select * from socialmedia.myweb_img;`
